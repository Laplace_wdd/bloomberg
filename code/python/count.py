#! /usr/bin/python
# --------------------------------------- 
# File Name : genSent.py
# Creation Date : 06-02-2014
# Last Modified : Thu Feb  6 19:50:28 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, numpy
from itertools import *

def procSent(input_f):
	cnt = 0
	for l in input_f:
		if l == "\n":
			yield cnt
			cnt = 0
		else:
			cnt += 1
	yield cnt

def main():
	vec = []	
	for cnt in procSent(sys.stdin):
		vec += [cnt]
	print "Avgerage length: " + str(numpy.mean(vec))
	
if __name__ == "__main__": main()
