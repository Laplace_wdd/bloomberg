#========================================================
bstrn=64
itr=60
sec=23
ver_trn=0.6
ver_tst=
lang=english
usage=test
#=========================================================
path=/proj/mlnlp/wdd/depParsing
tester_raw=${path}/zpar${ver_tst}/dist${bstst}/zpar.en
tester_tagged=${path}/zpar${ver_tst}/dist${bstst}/english.depparser/depparser
#tester=${path}/zpar${ver}/dist/parser.en/parser
test_raw_f=sec${sec}.sents
test_tagged_f=sec${sec}.tagged.zparformat
gold_f=sec${sec}.deps
config=${usage}.bstrn${bstrn}.bstst${bstst}.sec${sec}.ver${ver_trn}.${lang}.itr${itr}
local_path=${path}/workspace/${config}
model_in=${local_path}/model
tmp_dir=${local_path}/tmp
input_dir=${path}/data
output_dir=${local_path}/parser_output
depparser_mod=${local_path}/dpmodel
log_dir=${local_path}/log
if [ ! -d ${local_path} ]
then
	mkdir ${local_path}
	mkdir ${tmp_dir}
	mkdir ${output_dir}
	mkdir ${log_dir}
	mkdir ${depparser_mod}
	cp ${path}/models.en/english/depparser ${path}/models.en/english/conparser ${path}/models.en/english/tagger ${depparser_mod}/
fi
echo ${config}
for i in `seq 1 ${itr}`;
do
	echo Iteration $i
	echo Iteration $i >> ${log_dir}/eval.txt 
	echo Iteration $i >> ${log_dir}/tst_log.txt 
	rm -f ${depparser_mod}/depparser
	cp ${model_in}.$i ${depparser_mod}/depparser
	echo testing raw
	echo testing raw >> ${log_dir}/tst_log.txt
	echo testing raw >> ${log_dir}/eval.txt
	${tester_raw} ${depparser_mod} ${input_dir}/${test_raw_f} ${output_dir}/$i.zparout.raw >> ${log_dir}/tst_log.txt 
	python ${path}/evaluation/evaluate.py ${output_dir}/$i.zparout.raw ${input_dir}/${gold_f} > ${tmp_dir}/log.txt
	cat ${tmp_dir}/log.txt
	cat ${tmp_dir}/log.txt >> ${log_dir}/eval.txt
	echo testing tagged 
	echo testing tagged >> ${log_dir}/tst_log.txt
	echo testing tagged >> ${log_dir}/eval.txt
	${tester_tagged} ${input_dir}/${test_tagged_f} ${output_dir}/$i.zparout.tagged ${tmp_dir}/model >> ${log_dir}/tst_log.txt 
	python ${path}/evaluation/evaluate.py ${output_dir}/$i.zparout.tagged ${input_dir}/${gold_f} > ${tmp_dir}/log.txt
	cat ${tmp_dir}/log.txt
	cat ${tmp_dir}/log.txt >> ${log_dir}/eval.txt
done
