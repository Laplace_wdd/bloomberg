#! /usr/bin/python
# --------------------------------------- 
# File Name : genSent.py
# Creation Date : 06-02-2014
# Last Modified : Thu Feb 27 20:00:06 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, StringIO
from itertools import *

class TreeNode:

	__nodeMap, __cnt = {}, 0

	def __init__(self, tid, token = "", pos = "" , head = -2, arc = ""):
		self.tid = tid
		self.__set_value(token, pos, head, arc)
		self.children = []
		
	def __set_value(self, token, pos, head, arc):
		self.token = token
		self.pos = pos
		self.head = head
		self.arc = arc
		if head != -2:
			if head not in TreeNode.__nodeMap:
				head_node = TreeNode(head)
				TreeNode.__nodeMap[head] = head_node 
			TreeNode.__nodeMap[head].children += [self]

	@staticmethod
	def add_node(token, pos, head, arc):
		if TreeNode.__cnt in TreeNode.__nodeMap:
			TreeNode.__nodeMap[TreeNode.__cnt].__set_value(token, pos, head, arc)
		else:
			TreeNode.__nodeMap[TreeNode.__cnt] = TreeNode(TreeNode.__cnt, token, pos, head, arc)
		TreeNode.__cnt += 1

	@staticmethod
	def clear():
		TreeNode.__nodeMap, TreeNode.__cnt = {}, 0

	@staticmethod
	def empty():
		return TreeNode.__cnt == 0

	@staticmethod
	def dfs(node):
		if len(node.children) == 0:
			return [[node.arc]]
		all_path = []
		for chi in node.children:
			chi_lst = TreeNode.dfs(chi)
			for lst in chi_lst:
				lst += [node.arc]
			all_path += chi_lst
		return all_path

	@staticmethod
	def gen_path():
		assert len(TreeNode.__nodeMap[-1].children) == 1, "multiple roots"
		all_path = []
		root = TreeNode.__nodeMap[-1].children[0]
		for chi in root.children:
			all_path += TreeNode.dfs(chi)
		return '\n'.join([' '.join(lst) for lst in all_path]) 

def procSent(input_f):
	for l in input_f:
		if l == "\n":
			if not TreeNode.empty():
				yield TreeNode.gen_path() 
			TreeNode.clear()
		else:
			lt = l.split()
			TreeNode.add_node(lt[0], lt[1], int(lt[2]), lt[3])
	if not TreeNode.empty():
		yield TreeNode.gen_path() 

def main():
	for sent in procSent(sys.stdin):
		print sent
	
if __name__ == "__main__": main()
