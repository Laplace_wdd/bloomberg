import sys, re
def main():
	pos, arc = set(), set()
	p = re.compile('\s+')
	for l in sys.stdin:
		vec = p.split(l)
		if len(vec)==5:
			pos.add(vec[1])
			arc.add(vec[3])
	print "total POS:" + str(len(pos))
	print "total ARC:" + str(len(arc))
if __name__ == "__main__": main()
