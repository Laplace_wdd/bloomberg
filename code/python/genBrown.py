#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : genSent.py
# Creation Date : 06-02-2014
# Last Modified : Thu Feb 27 21:04:13 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, StringIO
from itertools import *
r2e_dict = {"_ROOT_":"_ROOT_"}
def main():
	for l in sys.stdin:
		lt = l.split()
		r2e_dict[lt[1]] = lt[0]
	pickle.dump( r2e_dict, open( "brown.dict", "wb" ) )
	
if __name__ == "__main__": main()
