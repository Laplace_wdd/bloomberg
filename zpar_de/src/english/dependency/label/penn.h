// Copyright (C) University of Oxford 2010
/****************************************************************
 *                                                              *
 * penn.h - the penn treebank style dependency labels           *
 *                                                              *
 * Author: Yue Zhang                                            *
 *                                                              *
 * Computing Laboratory, Oxford. 2008.07                        *
 *                                                              *
 ****************************************************************/

#ifndef _DEPENDENCY_L_PENN
#define _DEPENDENCY_L_PENN

#include "tags.h"

namespace english {

const std::string PENN_DEP_STRINGS[] = {
   "-NONE-",
   "ROOT", 
"PD|%",
"PAR",
"JU",
"AMS|",
"MNR%",
"SBP|",
"CC|",
"EP|",
"RS",
"SVP|",
"RE",
"OG|",
"AG|",
"CP|",
"RC",
"OA|",
"CJ|%",
"PG",
"SBP%",
"MNR|",
"AMS%",
"PAR%",
"PNC|",
"PH|",
"PH",
"SVP",
"PAR|",
"OA%",
"--",
"AG%",
"OC",
"OA",
"OG",
"OA2",
"CC%",
"CVC%",
"APP",
"OP",
"DA%",
"OA|%",
"RE|%",
"OP%",
"MO%",
"MNR",
"CC|%",
"CJ%",
"CM|",
"OC|",
"PD",
"OG%",
"NG|",
"AMS",
"DM|",
"PM",
"MNR|%",
"MO|",
"OP|",
"CVC|",
"EP",
"NG%",
"MO",
"OC%",
"SP%",
"CD|%",
"APP|%",
"CJ|",
"UC",
"AVC",
"OP|%",
"CD%",
"APP|",
"SB|%",
"DM",
"SB%",
"NK",
"RE|",
"NG",
"PD%",
"CJ",
"CM",
"CC",
"SB|",
"CD",
"ADC",
"CD|",
"APP%",
"CP",
"SBP",
"PD|",
"SP",
"MO|%",
"NMC",
"SB",
"NK|%",
"RE%",
"OC|%",
"NK%",
"RC|%",
"RS%",
"DA",
"--%",
"DA|",
"PG|",
"CVC",
"RC|",
"PAR|%",
"DA|%",
"JU|",
"--|",
"AC",
"PG%",
"AG",
"VO",
"NK|",
"OG|%",
"RC%",
"PNC"
};

enum PENN_DEP_LABELS {
   PENN_DEP_NONE=0,
   PENN_DEP_ROOT, 
PENN_DEP_PD__PATH,
PENN_DEP_PAR,
PENN_DEP_JU,
PENN_DEP_AMS_,
PENN_DEP_MNR_PATH,
PENN_DEP_SBP_,
PENN_DEP_CC_,
PENN_DEP_EP_,
PENN_DEP_RS,
PENN_DEP_SVP_,
PENN_DEP_RE,
PENN_DEP_OG_,
PENN_DEP_AG_,
PENN_DEP_CP_,
PENN_DEP_RC,
PENN_DEP_OA_,
PENN_DEP_CJ__PATH,
PENN_DEP_PG,
PENN_DEP_SBP_PATH,
PENN_DEP_MNR_,
PENN_DEP_AMS_PATH,
PENN_DEP_PAR_PATH,
PENN_DEP_PNC_,
PENN_DEP_PH_,
PENN_DEP_PH,
PENN_DEP_SVP,
PENN_DEP_PAR_,
PENN_DEP_OA_PATH,
PENN_DEP_MINMIN,
PENN_DEP_AG_PATH,
PENN_DEP_OC,
PENN_DEP_OA,
PENN_DEP_OG,
PENN_DEP_OA2,
PENN_DEP_CC_PATH,
PENN_DEP_CVC_PATH,
PENN_DEP_APP,
PENN_DEP_OP,
PENN_DEP_DA_PATH,
PENN_DEP_OA__PATH,
PENN_DEP_RE__PATH,
PENN_DEP_OP_PATH,
PENN_DEP_MO_PATH,
PENN_DEP_MNR,
PENN_DEP_CC__PATH,
PENN_DEP_CJ_PATH,
PENN_DEP_CM_,
PENN_DEP_OC_,
PENN_DEP_PD,
PENN_DEP_OG_PATH,
PENN_DEP_NG_,
PENN_DEP_AMS,
PENN_DEP_DM_,
PENN_DEP_PM,
PENN_DEP_MNR__PATH,
PENN_DEP_MO_,
PENN_DEP_OP_,
PENN_DEP_CVC_,
PENN_DEP_EP,
PENN_DEP_NG_PATH,
PENN_DEP_MO,
PENN_DEP_OC_PATH,
PENN_DEP_SP_PATH,
PENN_DEP_CD__PATH,
PENN_DEP_APP__PATH,
PENN_DEP_CJ_,
PENN_DEP_UC,
PENN_DEP_AVC,
PENN_DEP_OP__PATH,
PENN_DEP_CD_PATH,
PENN_DEP_APP_,
PENN_DEP_SB__PATH,
PENN_DEP_DM,
PENN_DEP_SB_PATH,
PENN_DEP_NK,
PENN_DEP_RE_,
PENN_DEP_NG,
PENN_DEP_PD_PATH,
PENN_DEP_CJ,
PENN_DEP_CM,
PENN_DEP_CC,
PENN_DEP_SB_,
PENN_DEP_CD,
PENN_DEP_ADC,
PENN_DEP_CD_,
PENN_DEP_APP_PATH,
PENN_DEP_CP,
PENN_DEP_SBP,
PENN_DEP_PD_,
PENN_DEP_SP,
PENN_DEP_MO__PATH,
PENN_DEP_NMC,
PENN_DEP_SB,
PENN_DEP_NK__PATH,
PENN_DEP_RE_PATH,
PENN_DEP_OC__PATH,
PENN_DEP_NK_PATH,
PENN_DEP_RC__PATH,
PENN_DEP_RS_PATH,
PENN_DEP_DA,
PENN_DEP_MINMIN_PATH,
PENN_DEP_DA_,
PENN_DEP_PG_,
PENN_DEP_CVC,
PENN_DEP_RC_,
PENN_DEP_PAR__PATH,
PENN_DEP_DA__PATH,
PENN_DEP_JU_,
PENN_DEP_MINMIN_,
PENN_DEP_AC,
PENN_DEP_PG_PATH,
PENN_DEP_AG,
PENN_DEP_VO,
PENN_DEP_NK_,
PENN_DEP_OG__PATH,
PENN_DEP_RC_PATH,
PENN_DEP_PNC,
   PENN_DEP_COUNT 
};

const unsigned long PENN_DEP_COUNT_BITS = 4;

/*==============================================================
 *
 * dependency lab
 *
 *==============================================================*/

class CDependencyLabel {

public:

   enum {NONE=0};
   enum {ROOT=1};
   enum {FIRST=1};
   enum {COUNT=PENN_DEP_COUNT};
   enum {MAX_COUNT=COUNT};
   enum {SIZE=PENN_DEP_COUNT_BITS};

protected:

   unsigned long m_code;

public:

   CDependencyLabel() : m_code(NONE) {}
   CDependencyLabel(const unsigned long &code) : m_code(code) { }
   CDependencyLabel(const std::string &str) { load(str); }
   virtual ~CDependencyLabel() {}

public:

   const unsigned long &hash() const { return m_code; }
   bool operator == (const CDependencyLabel &l) const { return m_code == l.m_code; }
   bool operator != (const CDependencyLabel &l) const { return m_code != l.m_code; }
   bool operator < (const CDependencyLabel &l) const { return m_code < l.m_code; }
   bool operator > (const CDependencyLabel &l) const { return m_code > l.m_code; }
   bool operator <= (const CDependencyLabel &l) const { return m_code <= l.m_code; }
   bool operator >= (const CDependencyLabel &l) const { return m_code >= l.m_code; }

   void load(const std::string &str) { 
      m_code = PENN_DEP_NONE;
      for (int i=FIRST; i<COUNT; ++i) {
         if (PENN_DEP_STRINGS[i]==str) {
            m_code = i;
            return;
         }
      }
   }

   void load(const unsigned long &u) { 
      m_code = u;
   }

   const std::string &str() const { 
      return PENN_DEP_STRINGS[ m_code ]; 
   }

   const unsigned long &code() const {
      return m_code;
   }

};

inline std::istream & operator >> (std::istream &is, CDependencyLabel &label) {
   std::string s;
   is >> s;
   label.load(s);
   return is;
}

inline std::ostream & operator << (std::ostream &os, const CDependencyLabel &label) {
   os << label.str() ;
   return os;
}
};

#endif
