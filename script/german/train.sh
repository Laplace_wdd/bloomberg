#========================================================
bstrn=64
itr=60
ver_trn=_de
lang=german
pproj=path
usage=train_${pproj}_d
type=german
#=========================================================
path=/proj/mlnlp/wdd/depParsing
trainer=${path}/zpar${ver_trn}/${pproj}/distd${bstrn}/english.depparser/train
train_f=train.752052.proj.${pproj}.-1
config=${type}.${usage}.bstrn${bstrn}.ver${ver_trn}.${lang}.itr${itr}
local_path=${path}/workspace/${config}
model_dir=${local_path}/model
tmp_dir=${local_path}/tmp
input_dir=${path}/german
log_dir=${local_path}/log
if [ ! -d ${local_path} ]
then
	mkdir ${local_path}
	mkdir ${model_dir}
	mkdir ${tmp_dir}
	mkdir ${log_dir}
fi
echo ${config}
for i in `seq 1 ${itr}`;
do
	echo Iteration $i
	echo Iteration $i >> ${log_dir}/log.txt 
	echo "${trainer} ${input_dir}/${train_f} ${tmp_dir}/model 1"
	${trainer} ${input_dir}/${train_f} ${tmp_dir}/model 1 > ${tmp_dir}/log.txt 
	cp ${tmp_dir}/model ${model_dir}/model.$i
	cat ${tmp_dir}/log.txt
	cat ${tmp_dir}/log.txt >> ${log_dir}/log.txt
done
