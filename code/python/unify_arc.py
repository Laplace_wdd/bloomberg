#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : genSent.py
# Creation Date : 06-02-2014
# Last Modified : Thu Feb 27 21:00:58 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, StringIO
from itertools import *

r2e_dict = pickle.load(open("brown.dict",'rb'))

def genTree(lst):
	res_io = StringIO.StringIO()
	global r2e_dict
	for tk, pos, head, arc in lst:
		print >> res_io, tk + " " + pos + " " + str(head) + " " + r2e_dict[arc] 
	res = res_io.getvalue()
	res_io.close()
	return res

def procSent(input_f):
	old_sent = [] 
	for l in input_f:
		if l == "\n":
			yield genTree(old_sent) 
			old_sent = [] 
		else:
			lt = l.split()
			old_sent += [[lt[0], lt[1], lt[2], lt[3]]] 
	if len(old_sent) > 0:
		yield genTree(old_sent) 

def main():
	for sent in procSent(sys.stdin):
		print sent
	
if __name__ == "__main__": main()
