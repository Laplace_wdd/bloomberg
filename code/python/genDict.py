#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : genSent.py
# Creation Date : 06-02-2014
# Last Modified : Thu Feb 27 21:20:44 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, StringIO
from itertools import *
r2e_dict = { 
#	'предик': 'SUBJ' 1101,
#	'1-компл': 'OBJ' 10,
#	'2-компл': 'OBJ' 10,
#	'3-компл': 'OBJ' 10,
#	'4-компл': 'OBJ' 10,
#	'5-компл': 'OBJ' 1101,
#	'опред': 'AMOD' 1100,
#	'предл': 'PREP' 0,
#	'обст': 'POBJ' 10,
#	'_ROOT_': '_ROOT_',
}

def genTree(lst):
	global r2e_dict
	for tk, pos, head, arc in lst:
		if arc not in r2e_dict:
			r2e_dict[arc] = 'ARC_' + str(len(r2e_dict))
	#		r2e_dict[arc] = 'MOD' 

def procSent(input_f):
	old_sent = [] 
	for l in input_f:
		if l == "\n":
			genTree(old_sent) 
			old_sent = [] 
		else:
			lt = l.split()
			old_sent += [[lt[0], lt[1], lt[2], lt[3]]] 
	if len(old_sent) > 0:
		genTree(old_sent) 

def main():
	procSent(sys.stdin)
	pickle.dump( r2e_dict, open( "full.dict", "wb" ) )
	
if __name__ == "__main__": main()
