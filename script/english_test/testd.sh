#!/bin/bash
#Config========================================================
bstrn=64
itr=60
ver_trn=0.5
bstst=1
lang=german
usage=dev_hard_code
type=english
#Declaration=========================================================
config=${type}.${usage}.bstrn${bstrn}.bstst${bstst}.ver${ver_trn}.${lang}.itr${itr}
path=/proj/mlnlp/wdd/depParsing
tester=${path}/zpar_de/dist/${type}.depparser/depparser
test_f=dev.zparinput
gold_f=dev.deps.-1
local_path=${path}/workspace/${config}
model_dir=${path}/workspace/generic.train.bstrn${bstrn}.ver${ver_trn}.${lang}.itr${itr}/model
tmp_dir=${local_path}/tmp
input_dir=${path}/german
output_dir=${local_path}/parser_output
log_dir=${local_path}/log
summary=${local_path}/summary
nwords=88100
#Work==============================================================
if [ ! -d ${local_path} ]
then
	mkdir ${local_path}
	mkdir ${tmp_dir}
	mkdir ${output_dir}
	mkdir ${log_dir}
	mkdir ${summary}
fi
echo ${config}
for i in `seq 1 ${itr}`;
do
	echo Iteration $i
	echo Iteration $i >> ${log_dir}/eval_proj.txt 
	echo Iteration $i >> ${log_dir}/eval_deproj.txt 
	echo Iteration $i >> ${log_dir}/tst_log.txt 
	date_start=$(date +%s)
	echo "Start Time:"$(date)
	echo "Start Time:"$(date) >> ${log_dir}/tst_log.txt
	${tester} ${input_dir}/${test_f} ${tmp_dir}/zparout.tagged ${model_dir}/model.${i} > ${tmp_dir}/tst_log.txt 
	cat ${tmp_dir}/tst_log.txt >> ${log_dir}/tst_log.txt
       	tld=$(perl -n -e '/Loading scores... done. \((.*)s\)/ && print "$1"'< ${tmp_dir}/tst_log.txt)
	echo -n ${tld}, >> ${summary}/time_load.csv
       	tt=$(perl -n -e '/Parsing has finished successfully. Total time taken is: (.*)/ && print $1'< ${tmp_dir}/tst_log.txt)
	echo -n ${tt}, >> ${summary}/time_total.csv
	echo -n $(echo "scale=2;${nwords}/${tld}"|bc), >> ${summary}/avg_time_load.csv
	echo -n $(echo "scale=2;${nwords}/${tt}"|bc), >> ${summary}/avg_time_total.csv
	export dpath=${tmp_dir}
	./deprojectivize.sh ${tmp_dir}/zparout.tagged ${output_dir}/$i.zparout.deproj #>> ${log_dir}/tst_log.txt 
	date_end=$(date +%s)
	echo "End Time:"$(date) 
	echo "End Time:"$(date) >> ${log_dir}/tst_log.txt
	echo "System Parsing Time:$((date_end-date_start))sec" 
	echo "System Parsing Time:$((date_end-date_start))sec" >> ${log_dir}/tst_log.txt 
	echo -n $((date_end-date_start)), >> ${summary}/time_system.csv	
	echo -n $(echo "scale=2;${nwords}/$((date_end-date_start))"|bc), >> ${summary}/avg_time_system.csv	
	cat ${tmp_dir}/zparout.tagged | sed 's/\t/ /g' | sed 's/-ROOT-/--/g' > ${output_dir}/$i.zparout.proj 
	#======================Projective=============================================================================
	python ${path}/evaluation/evaluate.py ${output_dir}/$i.zparout.proj ${input_dir}/${gold_f} > ${tmp_dir}/log.txt
	cat ${tmp_dir}/log.txt
	cat ${tmp_dir}/log.txt >> ${log_dir}/eval_proj.txt
       	perl -n -e '/Dependency precision without punctuations: (.*) .* .*/ && print "$1,"'< ${tmp_dir}/log.txt >>${summary}/eval_word_proj.csv
       	perl -n -e '/Sent precisions: (.*)/ && print "$1,"'< ${tmp_dir}/log.txt >>${summary}/eval_sent_proj.csv
	#======================Deprojective=============================================================================
	python ${path}/evaluation/evaluate.py ${output_dir}/$i.zparout.deproj ${input_dir}/${gold_f} > ${tmp_dir}/log.txt
        cat ${tmp_dir}/log.txt
        cat ${tmp_dir}/log.txt >> ${log_dir}/eval_deproj.txt
        perl -n -e '/Dependency precision without punctuations: (.*) .* .*/ && print "$1,"'< ${tmp_dir}/log.txt >>${summary}/eval_word_deproj.csv
        perl -n -e '/Sent precisions: (.*)/ && print "$1,"'< ${tmp_dir}/log.txt >>${summary}/eval_sent_deproj.csv
done
