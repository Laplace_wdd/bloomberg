// Copyright (C) University of Oxford 2010
/****************************************************************
 *                                                              *
 * tags.h - the definitions for english tags                    *
 *                                                              *
 * Author: Yue Zhang                                            *
 *                                                              *
 * Computing Laboratory, Oxford. 2007.10                        *
 *                                                              *
 ****************************************************************/

#ifndef _ENGLISH_PENN_POS_H
#define _ENGLISH_PENN_POS_H 1

namespace english {

/*===============================================================
 *
 * definitions abstd::cout tags 
 *
 * CTag is defined as unsigned long integer, which is easier to store.
 * The value of CTag is defined by the index in PENN_TAG_STRINGS + 1.
 *  
 *==============================================================*/

// the penn tag set
// Modify the following three constants together, keeping consistency!
const std::string PENN_TAG_STRINGS[] = {
   "-NONE-",
   "-BEGIN-",
   "-END-",
   "$.","KON","PWAT","PWAV","APPR",
   "VMFIN","ADJA","VAPP","ADJD","VVIZU",
   "APPO","PRF","NN","NNE","PDAT","NE",
   "PIAT","FM","VVINF","VAIMP","PTKVZ",
   "PRELS","PIS","VMPP","PROAV","VVFIN",
   "TRUNC","ADV","$,","PPOSAT","$(","PPOSS",
   "KOUS","PDS","VVPP","PTKANT","XY","PRELAT",
   "ITJ","PTKZU","CARD","VMINF","KOUI","VVIMP",
   "ART","PWS","VAFIN","PTKNEG","VAINF",
   "APPRART","KOKOM","PTKA","PPER","APZR"

};

enum PENN_TAG_CONSTANTS {
   PENN_TAG_NONE=0, 
   PENN_TAG_BEGIN,
   PENN_TAG_END,
   PENN_TAG_DOLLARDOT,PENN_TAG_KON,PENN_TAG_PWAT,PENN_TAG_PWAV,PENN_TAG_APPR,
   PENN_TAG_VMFIN,PENN_TAG_ADJA,PENN_TAG_VAPP,PENN_TAG_ADJD,PENN_TAG_VVIZU,
   PENN_TAG_APPO,PENN_TAG_PRF,PENN_TAG_NN,PENN_TAG_NNE,PENN_TAG_PDAT,PENN_TAG_NE,
   PENN_TAG_PIAT,PENN_TAG_FM,PENN_TAG_VVINF,PENN_TAG_VAIMP,PENN_TAG_PTKVZ,
   PENN_TAG_PRELS,PENN_TAG_PIS,PENN_TAG_VMPP,PENN_TAG_PROAV,PENN_TAG_VVFIN,
   PENN_TAG_TRUNC,PENN_TAG_ADV,PENN_TAG_DOLLARCOMMA,PENN_TAG_PPOSAT,PENN_TAG_DOLLARRB,PENN_TAG_PPOSS,
   PENN_TAG_KOUS,PENN_TAG_PDS,PENN_TAG_VVPP,PENN_TAG_PTKANT,PENN_TAG_XY,PENN_TAG_PRELAT,
   PENN_TAG_ITJ,PENN_TAG_PTKZU,PENN_TAG_CARD,PENN_TAG_VMINF,PENN_TAG_KOUI,PENN_TAG_VVIMP,
   PENN_TAG_ART,PENN_TAG_PWS,PENN_TAG_VAFIN,PENN_TAG_PTKNEG,PENN_TAG_VAINF,
   PENN_TAG_APPRART,PENN_TAG_KOKOM,PENN_TAG_PTKA,PENN_TAG_PPER,PENN_TAG_APZR,
   PENN_TAG_COUNT
};

const bool PENN_TAG_CLOSED[] = {
   false, 
   true,
   true,
   true,true,true,true,true,
   false,false,true,true,false,
   true,true,false,false,true,false,
   true,false,false,true,false,
   true,true,false,true,false,
   false,false,true,true,true,true,
   true,true,false,true,false,true,
   false,false,false,true,true,false,
   true,true,false,true,true,
   true,true,true,true,true,
};

const int PENN_TAG_FIRST = 3;
const int PENN_TAG_COUNT_BITS = 6;

/*
const unsigned long long PENN_TAG_MUST_SEE = (static_cast<unsigned long long>(1)<<PENN_TAG_SYM) | \
                                        (static_cast<unsigned long long>(1)<<PENN_TAG_FW) | \
                                        (static_cast<unsigned long long>(1)<<PENN_TAG_CD) | \
                                        (static_cast<unsigned long long>(1)<<PENN_TAG_LS) | \
                                        (static_cast<unsigned long long>(1)<<PENN_TAG_NOUN_PROPER) | \
                                        (static_cast<unsigned long long>(1)<<PENN_TAG_NOUN_PROPER_PLURAL) ;
*/
//===============================================================

class CTag {
public:
   enum {SENTENCE_BEGIN = PENN_TAG_BEGIN};
   enum {SENTENCE_END = PENN_TAG_END};
   enum {COUNT = PENN_TAG_COUNT};
   enum {MAX_COUNT = PENN_TAG_COUNT};
   enum {NONE = PENN_TAG_NONE};
   enum {SIZE = PENN_TAG_COUNT_BITS};
   enum {FIRST = PENN_TAG_FIRST};
   enum {LAST = PENN_TAG_COUNT-1};

protected:
   unsigned long m_code;

public:
   CTag() : m_code(NONE) {}
   CTag(PENN_TAG_CONSTANTS t) : m_code(t) { }
   CTag(int t) : m_code(t) { }
   CTag(const std::string &s) { load(s); }
   virtual ~CTag() {}

public:
   unsigned long code() const { return m_code; }
   unsigned long hash() const { return m_code; }
   void copy(const CTag &t) { m_code = t.m_code; }
   std::string str() const { assert(m_code<PENN_TAG_COUNT) ; return PENN_TAG_STRINGS[m_code]; }
   void load(const std::string &s) {
      m_code = PENN_TAG_NONE ;
      for (int i=1; i<PENN_TAG_COUNT; ++i)
         if (PENN_TAG_STRINGS[i] == s)
            m_code = i;
   }
   void load(const unsigned &i) {
      m_code = i;
   }
   bool closed() const { return PENN_TAG_CLOSED[m_code]; }
   //bool closed() const { return false; }

public:
   bool operator == (const CTag &t1) const { return m_code == t1.m_code; }
   bool operator != (const CTag &t1) const { return m_code != t1.m_code; }
   bool operator < (const CTag &t1) const { return m_code < t1.m_code; }
   bool operator > (const CTag &t1) const { return m_code > t1.m_code; }
   bool operator <= (const CTag &t1) const { return m_code <= t1.m_code; }
   bool operator >= (const CTag &t1) const { return m_code >= t1.m_code; }
};

//===============================================================
/*
inline unsigned matchPunctuation(const CTag &tag) {
   if (tag==PENN_TAG_L_QUOTE) return 1;
   else if (tag == PENN_TAG_L_BRACKET) return 2;
   else if (tag==PENN_TAG_R_BRACKET) return 4;
   else if (tag == PENN_TAG_COMMA) return 8;
   else if (tag == PENN_TAG_PERIOD) return 16;
   else if (tag == PENN_TAG_COLUM) return 32;
   else if (tag == PENN_TAG_R_QUOTE) return 64;
   else return 0;
}
*/
}; // namespace english

#endif

