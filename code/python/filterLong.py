#! /usr/bin/python
# --------------------------------------- 
# File Name : genSent.py
# Creation Date : 06-02-2014
# Last Modified : Thu Feb  6 19:50:28 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle
from itertools import *

def procSent(input_f):
	old_sent , cnt = "", 0
	for l in input_f:
		if l == "\n":
			yield cnt, old_sent
			old_sent , cnt = "", 0
		else:
			old_sent += l
			cnt += 1
	if len(old_sent) > 0:
		yield cnt, old_sent

def main():
	tl, tw, vl, vw = 0, 0, 0, 0
	for cnt, sent in procSent(sys.stdin):
		tl += 1
		tw += cnt 
		if cnt < 256:
			vl += 1
			vw += cnt
			print sent
	print >> sys.stderr, "total sentences: " + str(tl) + "\n" + "total words: " + str(tw) + "\n" + "valid sentences: " + str(vl) + "\n" + "valid words: " + str(vw) + "\n" 
	
if __name__ == "__main__": main()
