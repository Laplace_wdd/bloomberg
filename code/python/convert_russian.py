#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : convert.py
# Creation Date : 25-02-2014
# Last Modified : Wed Feb 26 02:10:34 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, StringIO
from itertools import *

phrase_node, tt_node = 0, 0
p_start = re.compile(r'<S ID="(\S+)">')
p_body = re.compile(r'.*<W DOM="(\S+)" FEAT="(\S+).*" ID="(\S+)" .*LINK="(\S+)">(.*)</W>')
p_root = re.compile(r'.*<W DOM="_root" FEAT="(\S+).*" ID="(\S+)".*>(.*)</W>')
p_phantom = re.compile(r'.*<W DOM="(\S+)" FEAT="(\S+).*" ID="(\S+)".* LINK="(\S+)" NODETYPE="FANTOM"/>')
p_rphantom = re.compile(r'<W DOM="_root" FEAT="(\S+).*" ID="(\S+)".* NODETYPE="FANTOM"/>')
p_end = re.compile(r'</S>')
p_linfo = re.compile(r'<LF LFARG=".*" LFFUNC=".*" LFVAL=".*"/>')
r2e_dict = {
	'предик': 'SUBJ',
	'1-компл': 'OBJ',
	'2-компл': 'OBJ',
	'3-компл': 'OBJ',
	'4-компл': 'OBJ',
	'5-компл': 'OBJ',
	'опред': 'AMOD',
	'предл': 'PREP',
	'обст': 'POBJ',
	'_ROOT_': '_ROOT_',
}


def genTree(lst):
	res_io = StringIO.StringIO()
	global tt_node, phrase_node, r2e_dict
	for tk, pos, head, arc in lst:
		tt_node += 1 
		tkl = re.split('\s+', tk)
		if len(tkl) > 1:
			phrase_node += 1
#		if arc not in r2e_dict:
#			display_arc = 'MOD' 
#		else:
#			display_arc = r2e_dict[arc] 
		if arc not in r2e_dict:
			r2e_dict[arc] = 'ARC_' + str(len(r2e_dict))
		display_arc = r2e_dict[arc]
		print >> res_io, tkl[0] + " " + pos + " " + str(head) + " " + display_arc 
		assert head < len(lst), str(head) + ":" + str(len(lst)) + ":index out of range"
	res = res_io.getvalue()
	res_io.close()
	return res

def procSent(input_f):
	in_sent, cnt, itm_lst = 0, 0, []
	for l in input_f:
		m_start = p_start.match(l)
		m_root = p_root.match(l)
		m_body = p_body.match(l)
		m_phantom = p_phantom.match(l)
		m_end = p_end.match(l)
		m_linfo = p_linfo.match(l)
		m_rphantom = p_rphantom.match(l)
		if m_start:
			assert in_sent == 0, l + ':last has not ended!'
			in_sent = 1
		elif m_end:
			assert in_sent == 1, l + ':sentence has not started!' 
			assert cnt > 0, l + ':no words found' 
			yield genTree(itm_lst)
			in_sent, cnt, itm_lst = 0, 0, []
		elif m_root:
			assert in_sent == 1, l + ':root not in sentence'
			pos = m_root.group(1)
			tid = int(m_root.group(2)) - 1
			assert tid == cnt, l + ': id does not match'
			tk = m_root.group(3)
			itm_lst += [[tk, pos, -1, "_ROOT_" ]]
			cnt += 1
		elif m_rphantom:
			assert in_sent == 1, l + ':root not in sentence'
			pos = m_rphantom.group(1)
			tid = int(m_rphantom.group(2)) - 1
			assert tid == cnt, l + ': id does not match'
			itm_lst += [["_FANTOM_", pos, -1, "_ROOT_" ]]
			cnt += 1	
		elif m_body:
			assert in_sent == 1, l + ':not in sentence'
			head = int(m_body.group(1)) - 1
			pos = m_body.group(2)
			tid = int(m_body.group(3)) - 1
			assert tid == cnt, l + ': id does not match'
			arc = m_body.group(4)
			tk = m_body.group(5)
			itm_lst += [[tk, pos, head, arc]]
			cnt += 1
		elif m_phantom:
			assert in_sent == 1, l + ':not in sentence'
			head = int(m_phantom.group(1)) - 1
			pos = m_phantom.group(2)
			tid = int(m_phantom.group(3)) - 1
			assert tid == cnt, l + ': id does not match'
			arc = m_phantom.group(4)
			itm_lst += [["_FANTOM_", pos, head, arc]]
			cnt += 1
		elif m_linfo:
			assert in_sent == 1, l + ':sentence has not started!'
		else:
			assert in_sent == 0, l + 'not correct format!'
	if in_sent == 1:
		yield genTree(itm_lst)

def main():
	for sent in procSent(sys.stdin):
		print sent
	print >> sys.stderr, 'Total token:' + str(tt_node)
	print >> sys.stderr, 'Phrase token:' + str(phrase_node)

if __name__ == "__main__": main()
