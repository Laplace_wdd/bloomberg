#!/bin/bash
pipe_in=${dpath}/pipe_in
pipe_out=${dpath}/pipe_out
mkfifo ${pipe_in} ${pipe_out}
cat $1 | awk '{if(NF==4) {$3=$3+1;print} else printf("\n")}'| sed 's/ /\t/g' | sed 's/\t\t\t//g' > ${pipe_in} & 
java -jar /proj/mlnlp/wdd/depParsing/maltparser-1.7.1/maltparser-1.7.1.jar -c pproj -m deproj -i ${pipe_in} -o ${pipe_out} -of malttab -if malttab &
cat ${pipe_out} | awk '{if(NF==4) {$3=$3-1;print} else printf("\n")}' | sed 's/-ROOT-/--/g' > $2
rm ${pipe_in} ${pipe_out}

