#========================================================
bstrn=64
itr=60
sec=23
ver_trn=
ver_tst=
bstst=64
lang=english
usage=train_test
type=generic
#=========================================================
path=/proj/mlnlp/ry2239/depParsing
trainer=${path}/zpar${ver_trn}/dist${bstrn}/${type}.depparser/train
tester_tagged=${path}/zpar${ver_tst}/dist${bstst}/${type}.depparser/depparser
train_f=treebank.standardtraining.deps
test_tagged_f=sec${sec}.tagged.zparformat
gold_f=sec${sec}.deps
config=type${type}.${usage}.bstrn${bstrn}.bstst${bstst}.sec${sec}.ver${ver_trn}.english${english}.itr${itr}
local_path=${path}/workspace/${config}
model_dir=${local_path}/model
tmp_dir=${local_path}/tmp
input_dir=${path}/data
output_dir=${local_path}/parser_output
log_dir=${local_path}/log
if [ ! -d ${local_path} ]
then
	mkdir ${local_path}
	mkdir ${model_dir}
	mkdir ${tmp_dir}
	mkdir ${output_dir}
	mkdir ${log_dir}
fi
echo ${config}
for i in `seq 1 ${itr}`;
do
	echo Iteration $i
	echo Iteration $i >> ${log_dir}/eval.txt 
	echo Iteration $i >> ${log_dir}/tst_log.txt 
	echo training
	${trainer} ${input_dir}/${train_f} ${tmp_dir}/model 1 
	cp ${tmp_dir}/model ${model_dir}/model.$i
	echo testing
	echo testing >> ${log_dir}/eval.txt
	echo testing >> ${log_dir}/tst_log.txt
	${tester_tagged} ${input_dir}/${test_tagged_f} ${output_dir}/$i.zparout.tagged ${tmp_dir}/model >> ${log_dir}/tst_log.txt 
	python ${path}/evaluation/evaluate.py ${output_dir}/$i.zparout.tagged ${input_dir}/${gold_f} > ${tmp_dir}/log.txt
	cat ${tmp_dir}/log.txt
	cat ${tmp_dir}/log.txt >> ${log_dir}/eval.txt
done
