#! /usr/bin/python
# --------------------------------------- 
# File Name : genSent.py
# Creation Date : 06-02-2014
# Last Modified : Mon Feb 17 22:01:48 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, StringIO
from itertools import *

def splitPlus(s):
	res, state, b, e = [], 1, 0, 0
	while e < len(s):
		if s[e] != "+":
			state = 0
		elif state == 0:
			res += [s[b:e]]
			b = e + 1
			state = 1
		e += 1
	if e > b:
		res += [s[b:e]]
	return res
	
def findHeader(tkLst):
		idx = len(tkLst)-1
		while idx > 0:
			pos = tkLst[idx][1]
			if not (pos == "SF" or pos == "SP" or pos == "SS" or
				pos == "SE" or pos == "SO" or pos == "SW"):
				break
			tkLst[idx][2] = tkLst[idx][2]-2
			idx -= 1
		return idx, tkLst

def genString(phLst, tkLst):
	res_io = StringIO.StringIO() 
	for i in range(len(tkLst)):
		if(tkLst[i][3] == -1):
			tkLst[i][2] = -1
		elif(tkLst[i][3] > -1):
			tkLst[i][2] = phLst[tkLst[i][3]]
	for tk, pos, head, rep, arc in tkLst:
		print >> res_io, tk + " " + pos + " " + str(head) + " " + arc
	res = res_io.getvalue()
	res_io.close()
	return res;

p = re.compile(r'(.*)/([^/].*)')

def procSent(input_f):
	phLst, tkLst, cnt = [],[],0 
	for l in input_f:
		if l == "\n":
			yield genString(phLst, tkLst) 
			phLst, tkLst, cnt = [],[],0 
		else:
			lt = l.split()
			tka, header, arc = splitPlus(lt[2]), int(lt[5])-1, lt[6]
			arc_in = arc
			if header == -1:
				arc_in = "MOD"
			else:
				arc_in += "_IN"
			tmp_tkLst = []
			for tks in tka:
				tkm = p.match(tks)
				tk, pos = tkm.group(1), tkm.group(2)
				if tk == "": tk = "+"
				cnt += 1
				tmp_tkLst += [[tk, pos, cnt, -10, arc_in]]
			idx, tmp_tkLst = findHeader(tmp_tkLst) 
			tmp_tkLst[idx][3] = header
			tmp_tkLst[idx][4] = arc
			idx += len(tkLst)
			tkLst += tmp_tkLst;
			phLst += [idx]
	yield genString(phLst, tkLst) 

def main():
	for sent in procSent(sys.stdin):
		print sent
	
if __name__ == "__main__": main()
