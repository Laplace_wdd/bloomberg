#! /usr/bin/python
# --------------------------------------- 
# File Name : genSent.py
# Creation Date : 06-02-2014
# Last Modified : Thu Feb  6 19:50:28 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle
from itertools import *
from operator import itemgetter

def main():
	adict= {}
	for l in sys.stdin:
		la = l.split()
		if len(la) > 1:
			if not la[3] in adict:
				adict[la[3]] = 0
			adict[la[3]] += 1
	print "f\ttype"
	sorted_dict = sorted(adict.items(), key=itemgetter(1), reverse=True)
	for p, f in sorted_dict: 
		print str(f) + "\t" + str(p)

if __name__ == "__main__": main()
