#!/bin/bash
#Config========================================================
bstrn=64
itr=60
sec=23
ver_trn=
#bstst=1
lang=english
usage=test_raw
type=english
#Declaration=========================================================
path=/proj/mlnlp/wdd/depParsing
tester=${path}/zpar/dist${bstst}/zpar.en
test_f=sec${sec}.sents
gold_f=sec${sec}.deps
config=${type}.${usage}.bstrn${bstrn}.bstst${bstst}.sec${sec}.ver${ver_trn}.english${english}.itr${itr}
local_path=${path}/workspace/${config}
model_dir=/proj/mlnlp/wdd/depParsing/workspace/train_test.bstrn${bstrn}.bstst64.sec${sec}.ver${ver_trn}.${lang}.itr${itr}/model
tmp_dir=${local_path}/tmp
input_dir=${path}/data
output_dir=${local_path}/parser_output
depparser_mod=${local_path}/dpmodel
log_dir=${local_path}/log
summary=${local_path}/summary
nwords=56684
#Work==============================================================
if [ ! -d ${local_path} ]
then
	mkdir ${local_path}
	mkdir ${tmp_dir}
	mkdir ${output_dir}
	mkdir ${log_dir}
	mkdir ${summary}
	mkdir ${depparser_mod}
	cp ${path}/models.en/english/depparser ${path}/models.en/english/conparser ${path}/models.en/english/tagger ${depparser_mod}/
fi
echo ${config}
for i in `seq 1 ${itr}`;
do
	echo Iteration $i
	echo Iteration $i >> ${log_dir}/eval.txt 
	echo Iteration $i >> ${log_dir}/tst_log.txt 
	rm -f ${depparser_mod}/depparser
	cp ${model_dir}/model.$i ${depparser_mod}/depparser
	date_start=$(date +%s)
	echo "Start Time:"$(date)
	echo "Start Time:"$(date) >> ${log_dir}/tst_log.txt
	${tester} ${depparser_mod} ${input_dir}/${test_f} ${tmp_dir}/$i.zparout.raw > ${tmp_dir}/tst_log.txt 
	cat ${tmp_dir}/tst_log.txt >> ${log_dir}/tst_log.txt
       	tld=$(perl -n -e '/Loading scores... done. \((.*)s\)/ && print "$1"'< ${tmp_dir}/tst_log.txt)
	echo -n ${tld}, >> ${summary}/time_load.csv
       	tt=$(perl -n -e '/Parsing has finished successfully. Total time taken is: (.*)/ && print $1'< ${tmp_dir}/tst_log.txt)
	echo -n ${tt}, >> ${summary}/time_total.csv
	echo -n $(echo "scale=2;${nwords}/${tld}"|bc), >> ${summary}/avg_time_load.csv
	echo -n $(echo "scale=2;${nwords}/${tt}"|bc), >> ${summary}/avg_time_total.csv
	date_end=$(date +%s)
	echo "End Time:"$(date) 
	echo "End Time:"$(date) >> ${log_dir}/tst_log.txt
	echo "System Parsing Time:$((date_end-date_start))sec" 
	echo "System Parsing Time:$((date_end-date_start))sec" >> ${log_dir}/tst_log.txt 
	cat ${tmp_dir}/$i.zparout.raw | sed 's/\t/ /g' | sed 's/-ROOT-/ROOT/g' > ${output_dir}/$i.zparout.raw 
	rm -f ${tmp_dir}/$i.zparout.raw
	python ${path}/evaluation/evaluate.py ${output_dir}/$i.zparout.raw ${input_dir}/${gold_f} > ${tmp_dir}/log.txt
       	perl -n -e '/Dependency precision without punctuations: (.*) .* .*/ && print "$1,"'< ${tmp_dir}/log.txt >>${summary}/eval_word.csv
       	perl -n -e '/Sent precisions: (.*)/ && print "$1,"'< ${tmp_dir}/log.txt >>${summary}/eval_sent.csv
	echo -n $((date_end-date_start)), >> ${summary}/time_system.csv	
	echo -n $(echo "scale=2;${nwords}/$((date_end-date_start))"|bc), >> ${summary}/avg_time_system.csv	
	cat ${tmp_dir}/log.txt
	cat ${tmp_dir}/log.txt >> ${log_dir}/eval.txt
done
