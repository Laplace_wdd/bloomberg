// Copyright (C) University of Oxford 2010
/****************************************************************
 *                                                              *
 * penn.h - the penn treebank style dependency labels           *
 *                                                              *
 * Author: Yue Zhang                                            *
 *                                                              *
 * Computing Laboratory, Oxford. 2008.07                        *
 *                                                              *
 ****************************************************************/

#ifndef _DEPENDENCY_L_PENN
#define _DEPENDENCY_L_PENN

#include "tags.h"

namespace english {

const std::string PENN_DEP_STRINGS[] = {
   "-NONE-",
   "ROOT", 
"PAR",
"DM",
"JU",
"DA",
"PH",
"CVC",
"NK",
"CM",
"RS",
"MNR",
"NG",
"RE",
"PG",
"PD",
"RC",
"AMS",
"PM",
"CJ",
"AC",
"AG",
"CC",
"CD",
"ADC",
"CP",
"EP",
"SVP",
"SBP",
"--",
"MO",
"SP",
"OC",
"OA",
"OA2",
"OG",
"NMC",
"VO",
"SB",
"PNC",
"AVC",
"APP",
"UC",
"OP"
};

enum PENN_DEP_LABELS {
   PENN_DEP_NONE=0,
   PENN_DEP_ROOT, 
PENN_DEP_PAR,
PENN_DEP_DM,
PENN_DEP_JU,
PENN_DEP_DA,
PENN_DEP_PH,
PENN_DEP_CVC,
PENN_DEP_NK,
PENN_DEP_CM,
PENN_DEP_RS,
PENN_DEP_MNR,
PENN_DEP_NG,
PENN_DEP_RE,
PENN_DEP_PG,
PENN_DEP_PD,
PENN_DEP_RC,
PENN_DEP_AMS,
PENN_DEP_PM,
PENN_DEP_CJ,
PENN_DEP_AC,
PENN_DEP_AG,
PENN_DEP_CC,
PENN_DEP_CD,
PENN_DEP_ADC,
PENN_DEP_CP,
PENN_DEP_EP,
PENN_DEP_SVP,
PENN_DEP_SBP,
PENN_DEP_MINMIN,
PENN_DEP_MO,
PENN_DEP_SP,
PENN_DEP_OC,
PENN_DEP_OA,
PENN_DEP_OA2,
PENN_DEP_OG,
PENN_DEP_NMC,
PENN_DEP_VO,
PENN_DEP_SB,
PENN_DEP_PNC,
PENN_DEP_AVC,
PENN_DEP_APP,
PENN_DEP_UC,
PENN_DEP_OP,
   PENN_DEP_COUNT 
};

const unsigned long PENN_DEP_COUNT_BITS = 4;

/*==============================================================
 *
 * dependency lab
 *
 *==============================================================*/

class CDependencyLabel {

public:

   enum {NONE=0};
   enum {ROOT=1};
   enum {FIRST=1};
   enum {COUNT=PENN_DEP_COUNT};
   enum {MAX_COUNT=COUNT};
   enum {SIZE=PENN_DEP_COUNT_BITS};

protected:

   unsigned long m_code;

public:

   CDependencyLabel() : m_code(NONE) {}
   CDependencyLabel(const unsigned long &code) : m_code(code) { }
   CDependencyLabel(const std::string &str) { load(str); }
   virtual ~CDependencyLabel() {}

public:

   const unsigned long &hash() const { return m_code; }
   bool operator == (const CDependencyLabel &l) const { return m_code == l.m_code; }
   bool operator != (const CDependencyLabel &l) const { return m_code != l.m_code; }
   bool operator < (const CDependencyLabel &l) const { return m_code < l.m_code; }
   bool operator > (const CDependencyLabel &l) const { return m_code > l.m_code; }
   bool operator <= (const CDependencyLabel &l) const { return m_code <= l.m_code; }
   bool operator >= (const CDependencyLabel &l) const { return m_code >= l.m_code; }

   void load(const std::string &str) { 
      m_code = PENN_DEP_NONE;
      for (int i=FIRST; i<COUNT; ++i) {
         if (PENN_DEP_STRINGS[i]==str) {
            m_code = i;
            return;
         }
      }
   }

   void load(const unsigned long &u) { 
      m_code = u;
   }

   const std::string &str() const { 
      return PENN_DEP_STRINGS[ m_code ]; 
   }

   const unsigned long &code() const {
      return m_code;
   }

};

inline std::istream & operator >> (std::istream &is, CDependencyLabel &label) {
   std::string s;
   is >> s;
   label.load(s);
   return is;
}

inline std::ostream & operator << (std::ostream &os, const CDependencyLabel &label) {
   os << label.str() ;
   return os;
}
};

#endif
