#! /usr/bin/python
# --------------------------------------- 
# File Name : preprocess.py
# Creation Date : 06-02-2014
# Last Modified : Thu Feb  6 18:45:13 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, random
from itertools import *

def procSent(input_f):
	old_sent = ""
	for l in input_f:
		if l == "\n":
			yield old_sent
			old_sent = ""
		else:
			old_sent += l
	yield old_sent

def main():
	sents = []
	for sent in procSent(sys.stdin):
		sents += [sent]
	num = int(sys.argv[1])-1
	print >> sys.stdout, sents[num]
	
if __name__ == "__main__": main()
