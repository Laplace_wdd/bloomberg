#！ /bin/bash

#generate file and recompile
#input$1 is the training file
#command is ./sun.sh #trainingfile#
tar=de
python genTag.py $1 pos_old.h label_old.h
mv pos.h ../zpar_${tar}/src/english/pos/penn.h
mv label.h ../zpar_${tar}/src/english/dependency/label/penn.h
cp rules.h ../zpar_${tar}/src/english/dependency/rules/penn.h
cp tags.h ../zpar_${tar}/src/english/tags.h
#make clean -C ../zpar_de
#make english.depparser -C ../zpar_de
